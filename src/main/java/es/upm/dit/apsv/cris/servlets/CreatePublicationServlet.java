package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import es.upm.dit.apsv.cris.dao.PublicationDAO;
import es.upm.dit.apsv.cris.dao.PublicationDAOImplementation;
import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;

@WebServlet("/CreatePublicationServlet")
public class CreatePublicationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Researcher user = (Researcher) request.getSession().getAttribute("user");
		if(user.getId().equals("root")){
			String id = request.getParameter("id");
			String title = request.getParameter("title");
			String pName = request.getParameter("publicationName");
			String pDate = request.getParameter("publicationDate");
			String authors = request.getParameter("authors");
			PublicationDAO dao = PublicationDAOImplementation.getInstance();
			Publication pold = dao.read(id);
			Publication pnew = new Publication ();
			if(pold == null) {
				pnew.setId(id);
				pnew.setTitle(title);
				pnew.setPublicationName(pName);
				pnew.setPublicationDate(pDate);
				pnew.setAuthors(authors);
				pnew.setCiteCount(0);
				dao.create(pnew);
				response.sendRedirect(request.getContextPath() + "/AdminServlet");
				}
		}
		else {
			getServletContext().getRequestDispatcher("/LoginView.jsp").forward(request, response);
		}
	}

}
