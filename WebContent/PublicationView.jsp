<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Researcher</title>
<%@ include file="Header.jsp" %>
</head>
<body>
<c:if test="${user.id=='root'}">
 <h2>Update publications of researcher</h2>
 <a href="UpdateCitationsAPIServlet?id=${pub.id}"> ${pub.id}</a>
</c:if>
<table>
	<tr>
	<th>Id</th><th>Title</th><th>Publication name</th><th>Date</th><th>Authors</th><th>Cite count</th>
	</tr>
	<tr>
		<td>${pi.id}</a></td>
		<td>${pi.title}</td>
		<td>${pi.publicationName}</td>
		<td>${pi.publicationDate}</td>
		<td>${pi.authors}</td>
		<td>${pi.citeCount}</td>
	</tr>
</table>
</body>
</html>